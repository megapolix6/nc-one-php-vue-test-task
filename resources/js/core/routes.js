import NotFound     from './components/NotFound'
import Home         from './components/Home'

// Load modules routes dynamically.
const requireContext = require.context('../modules', true, /routes\.js$/)

const modules = requireContext.keys()
    .map(file =>
        [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)]
    )

let moduleRoutes = []

for(let i in modules) {
    moduleRoutes = moduleRoutes.concat(modules[i][1].routes)
}

export const routes = [
    {
        path: '/',
        component: Home,
        children: [
            ...moduleRoutes,
            {
                path: '*',
                component: NotFound,
                name: 'not_found',
                hidden: true
            }
        ]
    },
]

