import * as types from './types'
import {actions} from './actions'

export const store = {
    state: {
        users: [],
        usersMeta: [],
        usersLoading: true,
        showPaginator: false,
    },
    getters: {
        users: state => state.users,
        usersMeta: state => state.usersMeta,
        usersLoading: state => state.usersLoading,
        showPaginator: state => state.showPaginator,
    },
    mutations: {
        [types.USER_OBTAIN](state, users) {
            state.users = users
        },
        [types.USER_CLEAR](state) {
            state.users = []
        },
        [types.USER_SET_LOADING](state, loading) {
            state.usersLoading = loading
        },
        [types.USER_META](state, meta) {
            state.usersMeta = meta
        },
        [types.USER_SET_PAGINATE](state, show) {
            state.showPaginator = show
        },
    },
    actions
}
