import Tables from './components/Tables'

export const routes = [

    {
        path: '/',
        name: 'Table First',
        iconCls: 'el-icon-tickets',
        component: Tables,
        meta: {
            paginate: false,
        },
    },
    {
        path: '/second',
        name: 'Table Second',
        iconCls: 'el-icon-document',
        component: Tables,
        meta: {
            paginate: false,
            tableNames: ['Table first','Table second','Table thrd']
        },
    },
    {
        path: '/third',
        name: 'Table With Pagination',
        iconCls: 'el-icon-sort',
        component: Tables,
        meta: {
            paginate: true,
            tableNames: ['Table name','Another table name','Table?']
        },
    },

]
